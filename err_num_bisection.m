
clear
clc
close all

f1 = @(x) x.^3 - 3*x.^2 - 3*x + 11;
a1 = -5;
b1 = 4;

x1 = fzero(f1, [a1 b1]);
eps = 1e-15;
[arr1, n1, c1] = methodBisection(f1, a1, b1, eps);
err1 = log10(abs(arr1 - x1));


f2 = @(x) cos(x) - x;
a2 = 0;
b2 = 3;

x2 = fzero(f2, [a2 b2]);
eps = 1e-15;
[arr2, n2, c2] = methodBisection(f2, a2, b2, eps);
err2 = log10(abs(arr2 - x2));


f3 = @(x) (x-10)./(x-15);
a3 = 5;
b3 = 14;

x3 = 10;
eps = 1e-15;
[arr3, n3, c3] = methodBisection(f3, a3, b3, eps);
err3 = log10(abs(arr3 - x3));

figure
hold all
grid on
plot(1:n1, err1(1:n1), "b*-"); %(1:n) - чтобы только те значения которые посчитаны, так как остальные - 0
plot(1:n2, err2(1:n2), "g*-");
plot(1:n3, err3(1:n3), "r*-");
title("Зависимость отклонения от номера итерации для функций" , "Fontsize", 24);
xlabel("Номер итераций n", "Fontsize", 18);
ylabel("Отклонение y = log_{10}|x_n - x*|", "Fontsize", 18);
legend("Отклонение для f1", "Отклонение для f2", "Отклонение для f3")
