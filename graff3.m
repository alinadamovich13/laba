clc, clear all, close all;
x = linspace(-10, 20);
f3 = (x-10)./(x-15);
plot(x, f3);
grid minor
xlabel('x', 'Color','r')
ylabel('y', 'Color','b')
title('График функции f3(x)=(x-10)/(x-15)')
legend('(x-10)/(x-15)')