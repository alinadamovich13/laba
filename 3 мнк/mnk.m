clc, clear all, close all
a0 = -5; 
b0 = 5; 
x = a0:0.2:b0; %50 точек
y = 2 .^ sin(x)+0.5*rand(size(x)); %исходная функция
plot(x,y,'b');
grid on