clc, clear all, close all;
x = 14.7:0.1:15.3;
f = (x+5) .* (x-15) .* (x-15.07);
figure
plot(x, f);
xlabel('x', 'Color','r')
ylabel('y', 'Color','b')
title('График функции f(x) = (x+5)(x-15)(x-15.07)')
legend('(x+15)(x-15)(x-15.07)')
grid on