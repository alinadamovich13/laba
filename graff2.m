clc, clear all, close all;
x = linspace(-1, 4);
f = cos(x) - x;
plot(x, f);
grid minor
xlabel('x', 'Color','r')
ylabel('y', 'Color','b')
title('График функции 2')
legend('cos(x) - x')