clc, clear all, close all;
y1 = @(x) x.^3 - 3*x.^2 - 3*x + 11;
x01 = [-5 4];
z1 = fzero(y1, x01)
format long;
options = optimset('TolX', 0.0000001);
[x fval exitflag output] = fzero(y1,x01,options)

clc, clear all, close all;
y2 = @(x) cos(x) - x;
x02 = [0 3];
z2 = fzero(y2, x02)
format long;
options = optimset('TolX', 0.0000001);
[x fval exitflag output] = fzero(y2,x02,options)

clc, clear all, close all;
y3 = @(x) (5 - x)./(x+5);
x03 = [4 14];
z3 = fzero(y3, x03)
format long;
options = optimset('TolX', 0.0000001);
[x fval exitflag output] = fzero(y3,x03,options)