clear
clc
close all


A = rand(10);
A = A' * A;
[u, s, v] = svd(A);
d = eye(10);
d(1, 1) = 10;
M1 = u*d*v';
c1 = cond(M1);
x = ones(10, 1);
b = M1 * x;

%количество итераций от параметра релаксации
figure
omega = linspace(0, 2, 21);
for i = 2:length(omega)-1
    [y, k(i-1)] = SOR(M1, b, omega(i), 1e-6);
end
hold all
grid on
plot(omega(2:end-1), k, ".-");
title("График зависимости количества итераций от параметра релаксации w")
xlabel("w")
ylabel("Число итераций k")

%зависимость погрешности от точности
omega = 1.5;

figure
eps = 10.^linspace(-1, -13, 13);
for i = 1:length(eps)
    [y, k] = SOR(M1, b, omega, eps(i));
    err(i) = norm(x - y, inf);
end
grid on
hold all
%plot(1:13, -1:-1:-13, "bo-");
plot(1:13, log10(err), "o-");
title("График зависимости ошибки от точности для метода релаксации при w = 1.5")
xlabel("степень точности")
ylabel("Ошибка (погрешность)")

%количество итераций от параметра релаксации
figure
omega = linspace(0, 2, 201);
for i = 2:length(omega)-1
    [y, k(i-1)] = SOR(M1, b, omega(i), 1e-6);
end
hold all
grid on
plot(omega(2:end-1), k, ".-");
title("График зависимости количества итераций от параметра релаксации w")
xlabel("w")
ylabel("Число итераций k")

%зависимость количества итераций от заданной точности
figure
k = zeros([13, 1]);
w = 1.5;
eps = 10.^linspace(-1, -13, 13);
for i = 1:length(eps)
    [r, k(i)] = SOR(M1, b, w, eps(i));
end
k, eps
hold all
grid on
plot(-log10(eps), k, "o-")
title("Зависимость количества итераций от заданной точности для метода релаксации")
xlabel("Точность eps = 10^(-n)")
ylabel("Число итераций k")


%погрешность от омега
figure
eps = 1e-6;
omega = linspace(0, 2, 201);
err = zeros([length(omega), 1]);
for i = 2:length(omega)-1
    [y, k] = SOR(M1, b, omega(i), eps);
    err(i) = norm(x - y, inf);
end
res = err', omega
hold all
grid on
plot(omega, res, ".-");
title("График зависимости погрешности от параметра релаксации w")
xlabel("w")
ylabel("Погрешность (ошибка)")

