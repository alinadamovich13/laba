clear
clc
close all

A = rand(10);
A = A' * A;
[u, s, v] = svd(A);
d = eye(10);
d(1, 1) = 10;
M1 = u*d*v';
c1 = cond(M1);
x = ones(10, 1);
b = M1 * x;

%уменьшение погрешности с ходом итераций
omega = 0.5;
num = 10;
eps = 1e-6;


figure

[y, k, err] = SOR(M1, b, omega, eps);


plot(1:num, err, "o-");
title("График зависимости ошибки от количества итерация для метода релаксации при w = 0.5")
xlabel("номер итерации k")
ylabel("Ошибка (погрешность)")
grid on