function[x2, k, err] = SOR(A, b, omega, eps)
% Решение Ax = b
n = length(b);

L = tril(A, -1); % нижняя треугольная матрица.
D = tril(A) - L; 
R = A - L - D;
B = -(D + omega*L)\eye(n)*(omega * R +(omega - 1) * D);
cst = norm((eye(n) - B)\eye(n))*norm(B);

x1 = zeros([n, 1]);
x2 = zeros([n, 1]);
k = 0;
flag = true;
% err = [];

while(flag)
    err = [];
    for i = 1:n
        %err =[];
        %iter = [];
        s = 0;
        for j = 1:1:(i-1)
            s = s + A(i, j)*x2(j);
        end

        for j = (i+1):n
            s = s + A(i, j)*x1(j);
        end
        x2(i) = (1 - omega)*x1(i) + omega/A(i,i)*(b(i) - s);
        err(i)=norm(x1 - x2);
        %iter(i) = i;
    end
    k = k + 1;
    flag = norm(x1-x2)> eps/cst; %условие выхода из цикла
    x1 = x2;
end
end