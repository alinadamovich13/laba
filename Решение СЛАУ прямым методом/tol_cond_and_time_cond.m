clc, close all, clear all

X = ones(10, 1); %вектор-столбец с 10-ю единицами, точное решение
%cond = 1
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 1; %задаем число обусловленности
M1 = u*d*v'; %свернули матрицу обратно
c1 = cond(M1);
B = M1*X; %умножили матрицу на вектор, получили правую часть
tic
[L1, U1] = LUdecomposition(M1);
K1 = LUsolver(L1, U1, B);
t1 = toc;
[L, U, P] = lu(M1);
Y0 = L\(P*B);
R = U\Y0;
h1 = norm(R - K1)

%cond = 10
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 10; %задаем число обусловленности
M1 = u*d*v';
c2 = cond(M1);
B = M1*X;
tic
[L2, U2] = LUdecomposition(M1);
K2 = LUsolver(L2, U2, B);
t2 = toc;
[L, U, P] = lu(M1);
Y0 = L\(P*B);
R = U\Y0;
h2 = norm(R - K2)

%cond = 100
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 100; %задаем число обусловленности
M1 = u*d*v';
c3 = cond(M1);
B = M1*X;
tic
[L3, U3] = LUdecomposition(M1);
K3 = LUsolver(L3, U3, B);
t3 = toc;
[L, U, P] = lu(M1);
Y0 = L\(P*B);
R = U\Y0;
h3 = norm(R - K3)


%cond = 1000
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 1000; %задаем число обусловленности
M1 = u*d*v';
c4 = cond(M1);
B = M1*X;
tic
[L4, U4] = LUdecomposition(M1);
K4 = LUsolver(L4, U4, B);
t4 = toc;
[L, U, P] = lu(M1);
Y0 = L\(P*B);
R = U\Y0;
h4 = norm(R - K4)

%cond = 100000
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 10000; %задаем число обусловленности
M1 = u*d*v';
c5 = cond(M1);
B = M1*X;
tic
[L5, U5] = LUdecomposition(M1);
K5 = LUsolver(L5, U5, B);
t5 = toc;
[L, U, P] = lu(M1);
Y0 = L\(P*B);
R = U\Y0;
h5 = norm(R - K5)

%cond = 100000
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 100000; %задаем число обусловленности
M1 = u*d*v';
c6 = cond(M1);
B = M1*X;
tic
[L6, U6] = LUdecomposition(M1);
K6 = LUsolver(L6, U6, B);
t6 = toc;
[L, U, P] = lu(M1);
Y0 = L\(P*B);
R = U\Y0;
h6 = norm(R - K6)

h = [h1 h2 h3 h4 h5 h6]
c = [c1 c2 c3 c4 c5 c6]
t = [t1 t2 t3 t4 t5 t6]
figure
loglog(c, h, ".-")
title("График зависимости ошибки от числа обусловленности для LU-метода", "FontSize",18)
xlabel("Число обусловленности", "FontSize", 14)
ylabel("Прологарифмированная норма отклонения", "FontSize", 14)
grid on

figure
semilogx(c, t)
title("График зависимости времени от числа обусловленности для LU-метода", "FontSize",18)
xlabel("Число обусловленности", "FontSize", 14)
ylabel("Прологарифмированное время", "FontSize", 14)
grid on