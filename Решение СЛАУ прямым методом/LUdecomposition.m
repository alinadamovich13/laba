function[l, u] = LUdecomposition(a)
[row, col] = size(a);
if(row ~= col) %если матрица не квадратная
    fprintf("ERROR\n");
    l = 0;
    u = 0;
    return
end
l = zeros([row, row]);
u = zeros([row, row]);
for i = 1:row
    u(1, i) = a(1, i);
    l(i, 1) = a(i, 1) / u(1, 1);
end

for i = 1:row
    l(i, i) = 1;
end

for i = 2:row
    for j = i:row
        s = 0;
        for k = 1:1:(i-1)
            s = s + l(i, k)*u(k, j);
        end

        u(i, j) = a(i, j) - s;

        if(i == j)
            continue
        end

        d = 0;
        for k = 1:(j-1)
            d = d + l(j, k)*u(k, i);
        end

        l(j, i) = (a(j, i) - d)/u(i, i);
    end
end
end
