function[x] = LUsolver(L, U, b)
%получили разложение матрицы А, теперь находим корни
% Ax = b -> LUx = b -> Ly = b -> Ux = y;
n = length(b);
y = zeros([1, n]);
x = zeros([1, n]);
y(1) = b(1);

for i = 2:n
    s = 0;
    for j = 1:(i-1)
        s = s + L(i, j) * y(j);
    end
    y(i) = b(i) - s;
end

x(n) = y(n)/U(n, n);

for i = (n-1):-1:1
    d = 0;
    for j = (i+1):n
        d = d + U(i, j) * x(j);
    end
    x(i) = (y(i) - d) / U(i, i);
end
end
