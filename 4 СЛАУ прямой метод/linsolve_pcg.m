clc, close all, clear all
A = [1 -2 1; 2 2 -1; 4 -1 1];
B = [0; 3; 5];
opts.UT = true;
X1 = linsolve(A, B); %точное значение
X2 = linsolve(A, B, opts); %решение для верхнетреугольной матрицы
opts.TRANSA = true;
X3 = linsolve(A, B, opts); %решение для транспонированной матрицы

A = [1 -2 1; 2 2 -1; 4 -1 1];
B = [0; 3; 5]; %точное решение - 1 2 3
tol=1e-7;
maxit = 10;
X = pcg(A, B, tol);
n = norm(B-A*X)/norm(B);

format long

c = [];
iter = [];
for n = 1:10
    A = hilb(n);
    c(n) = cond(A);
    B = ones(n, 1);
    R = linsolve(A, B);
    tol = 1e-5;
    [X, flag, iter(n)] = pcg(A, B, tol); %flag = 0, то значит все хорошо, если flag<0, то relres<tol
 
end
c
semilogx(c, iter, "o-")
title("Зависимость количество итераций от cond", "FontSize", 18)
xlabel("Число обусловленности матрицы", "FontSize", 14)
ylabel("Количество итераций", "FontSize", 14)
grid on
%количество итераций от cond
