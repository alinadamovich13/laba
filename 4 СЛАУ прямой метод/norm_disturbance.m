clc, close all, clear all
format long
%относительная погрешность = норма (х с возмущением - хточное)/норма(х точное) от возмущения

X = ones(10, 1); %вектор-столбец с 10-ю единицами
%cond = 1
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 1; %задаем число обусловленности
M1 = u*d*v'; %свернули матрицу обратно
c1 = cond(M1);
B = M1*X; %умножили матрицу на вектор, получили правую часть

[L, U, P] = lu(M1);
Y = L\(P*B);
R0 = U\Y; % исходное решение

X1 = X + 0.01*X;
B1 = M1*X1;
[L, U, P] = lu(M1);
Y = L\(P*B1);
R1 = U\Y;
ERR1 = norm(R1 - R0)/norm(R0)

X2 = X + 0.02*X;
B2 = M1*X2;
[L, U, P] = lu(M1);
Y = L\(P*B2);
R2 = U\Y;
ERR2 = norm(R2 - R0)/norm(R0)

X3 = X + 0.03*X;
B3 = M1*X3;
[L, U, P] = lu(M1);
Y = L\(P*B3);
R3 = U\Y;
ERR3 = norm(R3 - R0)/norm(R0)

r = [1 2 3];
ERR = [ERR1 ERR2 ERR3];
plot(ERR, r, "o-")
grid on
