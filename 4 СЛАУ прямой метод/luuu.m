clc, close all, clear all
format long
% A*X = B => X = A\B
% LU разложение
% n = 5;
% A = rand(n);
% X = ones(n, 1); % вектор-столбец с n единицами
% B = A*X;
% [t1, res1] = method_lu(A, B, n);
% t1, n

t = [];

for n = 1:350
    A = rand(n); 
    X = ones(n, 1); % вектор-столбец с n единицами
    B = A*X; %правая часть
    tic %начинаем отсчет
    [L, U, P] = lu(A);
    Y = L\(P*B);
    RES = U\Y;
    [t(n)] = toc; %получаем время
end
t
n=[1:350];

plot(n, t)
title("График зависимости временных затрат на решение СЛАУ LU-методом от размера матрицы", "FontSize",18)
xlabel("Размер матрицы n", "FontSize", 14)
ylabel("Временные затраты на решение СЛАУ t", "FontSize", 14)
grid on
