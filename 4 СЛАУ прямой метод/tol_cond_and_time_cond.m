clc, close all, clear all

X = ones(10, 1); %вектор-столбец с 10-ю единицами, точное решение
%cond = 1
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 1; %задаем число обусловленности
M1 = u*d*v'; %свернули матрицу обратно
c1 = cond(M1);
B = M1*X; %умножили матрицу на вектор, получили правую часть
tic
[L, U, P] = lu(M1);
Y = L\(P*B);
R = U\Y;
t1 = toc;
h1 = norm(R - X)

%cond = 10
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 10; %задаем число обусловленности
M1 = u*d*v';
c2 = cond(M1);
B = M1*X;
tic
[L, U, P] = lu(M1);
Y = L\(P*B);
R = U\Y;
t2 = toc;
h2 = norm(R - X)

%cond = 100
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 100; %задаем число обусловленности
M1 = u*d*v';
c3 = cond(M1);
B = M1*X;
tic
[L, U, P] = lu(M1);
Y = L\(P*B);
R = U\Y;
t3 = toc;
h3 = norm(R - X)


%cond = 1000
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 1000; %задаем число обусловленности
M1 = u*d*v';
c4 = cond(M1);
B = M1*X;
tic
[L, U, P] = lu(M1);
Y = L\(P*B);
R = U\Y;
t4 = toc;
h4 = norm(R - X)

%cond = 100000
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 10000; %задаем число обусловленности
M1 = u*d*v';
c5 = cond(M1);
B = M1*X;
tic
[L, U, P] = lu(M1);
Y = L\(P*B);
R = U\Y;
t5 = toc;
h5 = norm(R - X)

%cond = 100000
A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 100000; %задаем число обусловленности
M1 = u*d*v';
c6 = cond(M1);
B = M1*X;
tic
[L, U, P] = lu(M1);
Y = L\(P*B);
R = U\Y;
t6 = toc;
h6 = norm(R - X)

h = [h1 h2 h3 h4 h5 h6]
c = [c1 c2 c3 c4 c5 c6]
t = [t1 t2 t3 t4 t5 t6]
figure
loglog(c, h, ".-")
title("График зависимости точности от числа обусловленности для LU-метода", "FontSize",18)
xlabel("Число обусловленности", "FontSize", 14)
ylabel("Прологарифмированная норма отклонения", "FontSize", 14)
grid on

figure
semilogx(c, t)
title("График зависимости времени от числа обусловленности для LU-метода", "FontSize",18)
xlabel("Число обусловленности", "FontSize", 14)
ylabel("Прологарифмированное время", "FontSize", 14)
grid on