%зависимость отклонения от номера итерации
clear
clc
close all

format long

eps = 10^-15;

f1 = @(x) x.^3 - 3*x.^2 - 3*x + 11; %сама функция
diffF1 = @(x) 3*x.^2 - 6*x - 3; %ее производная
x0_1 = 3;
x1 = fzero(f1, x0_1); %находим истинное значение

f2 = @(x) cos(x)-x; %сама функция
diffF2 = @(x) -sin(x)-1; %ее производная
x0_2 = 1;
x2 = fzero(f2, x0_2); %находим истинное значение


f3 = @(x) (x-10)./(x-15); %сама функция
diffF3 = @(x) -5 ./ ((x-15) .^2); %ее производная
x0_3 = 13;
x3 = 10; %находим истинное значение


[x1_res, n1, arr1] = methodNewton(f1, diffF1, x0_1, eps);
err1 = log10((abs(arr1 - x1)));

[x2_res, n2, arr2] = methodNewton(f2, diffF2, x0_2, eps);
err2 = log10((abs(arr1 - x2)));

[x3_res, n3, arr3] = methodNewton(f3, diffF3, x0_3, eps);
err3 = log10((abs(arr1 - x3)));

figure
hold all
grid on
plot(1:n1, err1(1:n1), "b*--");
plot(1:n2, err2(1:n2), "g*--");
plot(1:n3, err3(1:n3), "r*--");
title("Зависимость отклонения от номера итерации при точности равной 10^{-15}", "Fontsize", 15);
xlabel("Номер итерации", "Fontsize", 15);
ylabel("Прологарифмированное отклонение", "Fontsize", 15);
legend("Отклонение для функции f1(x)","Отклонение для функции f2(x)","Отклонение для функции f3(x)")



