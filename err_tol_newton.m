clear
clc
close all
format long
eps = 10.^(-(1:1:15));

f1 = @(x) x.^3 - 3*x.^2 - 3*x + 11; %сама функция
diffF1 = @(x) 3*x.^2 - 6*x - 3; %ее производная
x01 = -2;
x1 = fzero(f1, x01); %находим истинное значение

f2 = @(x) cos(x)-x; %сама функция
diffF2 = @(x) -sin(x)-1; %ее производная
x02 = 0;
x2 = fzero(f2, x02); %находим истинное значение

f3 = @(x) (x-10)./(x-15); %сама функция
diffF3 = @(x) -5 ./ ((x-15).^2); %ее производная
x03 = 7;
x3 = 10; %находим истинное значение



err1 = zeros(1,length(eps)); %нулевой массив для отклонения
res1 = zeros(1,length(eps)); %нулевой массив для результата
err2 = zeros([1, length(eps)]); %нулевой массив для отклонения
res2 = zeros([1, length(eps)]); %нулевой массив для результата
err3 = zeros([1, length(eps)]); %нулевой массив для отклонения
res3 = zeros([1, length(eps)]); %нулевой массив для результата

for i = 1:length(eps)
    [res1(i)] = methodNewton(f1, diffF1, x01, eps(i)); %вызываем функцию, она возвращает результат и число итераций
     err1(i) = abs(res1(i) - x1);
     [res2(i)] = methodNewton(f2, diffF2, x02, eps(i)); %вызываем функцию, она возвращает результат и число итераций
     err2(i) = abs(res2(i) - x2);
     [res3(i)] = methodNewton(f3, diffF3, x03, eps(i)); %вызываем функцию, она возвращает результат и число итераций
     err3(i) = abs(res3(i) - x3);
end

figure
loglog(eps, err1, "b*--");
hold on
loglog(eps, err2, "g*--");
hold on
loglog(eps, err3, "r*--");
title("Зависимость погрешностей от заданной точности" , "Fontsize", 15);
xlabel("eps = 10^{-x}", "Fontsize", 15);
ylabel("\delta", "Fontsize", 15);

grid on

 x = 1e-15:0.000001:1;
 y = @(x) x;
 loglog(x,y(x))
 legend('для f1','для f2','для f3','y = x')

