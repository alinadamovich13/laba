close all
clc
clear all

format long


err1=[];
err2=[];
a = -5;
b = 4;
eps = 10.^-15;
x0 = 3;

for j = 0:5
    f = @(x) r(j)*x.^3 - 3*x.^2 - 3*x + 11; %вносим возмущение в функцию
    difF= @(x) r(j)*3*x.^2 - 6*x-3; %вносим возмущение в ее производную

    [abc,n, res1, dd] = methodBisection(f, a, b, eps) %результаты для бисекции
    [res2,n] = methodNewton(f, difF, x0, eps)%результаты для Ньютона

    err1 = [err1 res1];
    err2 = [err2 res2];


    if j == 0 %истинные значения
        r1 = fzero(f, [a b])
        r2 = fzero(f, [a b])
    end

end


figure
err1_rel = (abs(err1-r1))./(abs(r1)); %относительная погрешность
coeff = 0:5;
plot(coeff, err1_rel)
title('График зависимости относительной погрешности от величины возмущения для функции f1 методом половинного деления', "Fontsize", 18)
xlabel('процент возмущения')
ylabel('относительная погрешность')

grid on
hold all

figure
err2_rel = abs(err2-r2)./abs(r2); %относительная погрешность
plot(coeff, err2_rel)
title('График зависимости относительной погрешности от величины возмущения для функции f1 методом Ньютона', "Fontsize", 18)
xlabel('Процент возмущения', "Fontsize", 14)
ylabel('Относительная погрешность', "Fontsize", 14)

grid on
hold on
%---------------

function [disturbance]= r(coeff)
if coeff  == 0
    disturbance = 1;
else
    disturbance = 1 + sign(randi([-1,1]))*(rand()/100+(coeff-1)/100);
    if disturbance == 1
        [disturbance] = r(coeff);
    end
end
end

