function y = f(x)
y = 2.^(sin(x));
end
