clear
clc
close all
format long

f1 = @(x) x.^3 - 3*x.^2 - 3*x + 11;
a = -5;
b = 4;
x1 = fzero(f1, [a b]);
yy = 1:1:15;
xx = 1:1:15;

eps = 10.^(-1:-1:-15);
err1 = zeros([1, length(eps)]); %нулевой массив для отклонения
for i = 1:length(eps)
    [abr, n, c] = methodBisection(f1, a, b, eps(i)); %вызываем функцию, она возвращает результат и число итераций
    err1(i) = log10(abs(c - x1)); %записываем отклонение от истинного значения
end

%----------------------------
f2 = @(x) cos(x)-x;
a = 0;
b = 3;
x2 = fzero(f2, [a b]);


eps = 10.^(-1:-1:-15);
err2 = zeros([1, length(eps)]); %нулевой массив для отклонения
for i = 1:length(eps)
    [abr, n, c] = methodBisection(f2, a, b, eps(i)); %вызываем функцию, она возвращает результат и число итераций
    err2(i) = log10(abs(c - x2)); %записываем отклонение от истинного значения
end

%----------------------------
f3 = @(x) (x-10)./(x-15);
a = 5;
b = 14;
x3 = 10;


eps = 10.^(-1:-1:-15);
err3 = zeros([1, length(eps)]); %нулевой массив для отклонения
for i = 1:length(eps)
    [abr, n, c] = methodBisection(f3, a, b, eps(i)); %вызываем функцию, она возвращает результат и число итераций
    err3(i) = log10(abs(c - x3)); %записываем отклонение от истинного значения
end

figure
hold all
grid on
plot(1:15, err1, "b*--");
plot(1:15, err2, "g*--");
plot(1:15, err3, "r*--");
plot(yy, xx);
title("Зависимость погрешности от заданной точности для функций" , "Fontsize", 24);
xlabel("eps = 10^{-x}", "Fontsize", 24);
ylabel("|x - x*| = log_{10}y", "Fontsize", 24);
legend("Число обращений к функции f1(x)","Число обращений к функции f2(x)","Число обращений к функции f3(x)")

