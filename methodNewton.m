%параметры - начальная функция, ее производная, х0, точность, функция
%выводит количество итераций и корень 
function[x1, n, arr] = methodNewton(f, diffF, x0, eps)
    x1 = x0;
    n = 0;
    arr = zeros();
if (f(x1+eps)*f(x1-eps))>0
     f1 = f(x1);
    df1 = diffF(x1);

    while(abs(f1/df1) > eps && n<1000)
        x1 = x1 - f1 / df1; %считаем новую точку
        n = n + 1;
        f1 = f(x1); %считаем значение в новой точке
        df1 = diffF(x1);
        arr(n) = x1;
    end
end
end
