%%иллюстрация работы интерполяции полинома Лагранжа, +поточечная 
clc, clear all, close all

%функция 2 с синусом
%равномерная сетка
x = -2:0.02:2;
XX = -2:0.02:2;
YY = zeros(size(XX));

f = @(x) 2 .^ (sin(x));
X = linspace(-2,2,5); %количество узлов = 5
Y = f(X);
for i = 1:length(XX)
    YY(i) = LagrangeP(XX(i),X,Y); %вычисляем полином
    e1 = (YY - f(x)); %записываем ошибку
end

figure
plot(XX,YY,'DisplayName','Лагранж');
grid on
hold all
plot(X,Y,'bo','DisplayName','5 узлов');
grid on
hold all


X = linspace(-2,2,7); %количество узлов = 7
Y = f(X);
for i = 1:length(XX)
    YY(i) = LagrangeP(XX(i),X,Y); %вычисляем полином
    e2 = (YY - f(x)); %записываем ошибку
end

plot(XX,YY,'DisplayName','Лагранж');
grid on
hold all
plot(X,Y,'go','DisplayName','7 узлов');
grid on
hold all


X = linspace(-2,2,10); %количество узлов = 10
Y = f(X);
for i = 1:length(XX)
    YY(i) = LagrangeP(XX(i),X,Y); %вычисляем полином
    e3 = (YY - f(x)); %записываем ошибку
end

plot(XX,YY,'DisplayName','Лагранж');
grid on
hold all
plot(X,Y,'ro','DisplayName','10 узлов');
grid on
hold all

plot(x,f(x),'DisplayName','f(x)');
title('Интерполяция полиномом Лагранжа для функции 2 р/м сетка');
xlabel("x")
ylabel("y")
legend
grid on

figure
plot(x, e1, 'b')
title("Поточечная ошибка полином Лагранжа для функции 2 р/м сетка")
xlabel("x")
ylabel("y")
grid on
hold all
plot(x, e2, 'g')
grid on
hold all
plot(x, e3, 'r')
grid on
hold all
legend("5 узлов", "7 узлов", "10 узлов")


%чебышевская сетка
f = @(x) 2 .^ (sin(x));
X1=-2:0.02:2;
Y1=f(X1);
n=5; %количество узлов = 5
a=-2; %начало отрезка
b=2; %конец отрезка
XX = [];
for k = 1:n
    i =(1./2).*(a+b) + (1./2).*(b-a).*cos( (2*k - 1 ) / (2*n) * pi);
    XX = [XX i];
end
Y=f(XX);
X1=-2:0.02:2;
YY = zeros(size(X1));
for i = 1 : length(X1)
    YY(i)=LagrangeP(X1(i), XX,Y);
    e1 = (YY - f(X1));
end

figure
plot(X1,YY,'DisplayName','Чебышев');
grid on
plot(XX,Y,'bo','DisplayName','5 узлы Чебышева');
legend
grid on
hold all

n=7; %количество узлов = 7
XX = [];
for k = 1:n
    i =(1./2).*(a+b) + (1./2).*(b-a).*cos( (2*k - 1 ) / (2*n) * pi);
    XX = [XX i];
end
Y=f(XX);
X1=-2:0.02:2;
YY = zeros(size(X1));
for i = 1 : length(X1)
    YY(i)=LagrangeP(X1(i), XX,Y);
    e2 = (YY - f(X1));
end
plot(X1,YY,'DisplayName','Чебышев');
grid on
plot(XX,Y,'go','DisplayName','7 узлов Чебышева');
legend
grid on
hold all

n=10; %количество узлов = 10
XX = [];
for k = 1:n
    i =(1./2).*(a+b) + (1./2).*(b-a).*cos( (2*k - 1 ) / (2*n) * pi);
    XX = [XX i];
end
Y=f(XX);
X1=-2:0.02:2;
YY = zeros(size(X1));
for i = 1 : length(X1)
    YY(i)=LagrangeP(X1(i), XX,Y);
    e3 = (YY - f(X1));
end
plot(X1,YY,'DisplayName','Чебышев');
grid on
plot(XX,Y,'ro','DisplayName','10 узлов Чебышева');
legend
grid on
hold all

plot(x,f(x), 'DisplayName','f(x)');
title('Интерполяция полиномом Лагранжа для функции 2 Чебышевская сетка');
xlabel("x")
ylabel("y")
legend
grid on

figure
plot(X1, e1, 'b')
title("Поточечная ошибка полином Лагранжа для функции 2 чебышевская сетка")
xlabel("x")
ylabel("y")
grid on
hold all
plot(X1, e2, 'g')
grid on
hold all
plot(X1, e3, 'r')
grid on
hold all
legend("5 узлов", "7 узлов", "10 узлов")


