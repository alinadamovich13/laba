clc, close all, clear all
%ошибка сетки со сгущением
xx = -2:0.02:2;
YY = zeros(size(xx));
E1 = [];
E2 = [];
N = 51; %колчество узлов
f = @(x) 2 .^ (sin(x));
x0 = -1.5;


for n = 5:6:N
    xp = Thicken(xx, x0, n);
    X = linspace(-2,2,n);
    Y = f(X);

    for i = 1:length(xx)
        yy = LagrangeP(xx,X,Y);
        yyt = LagrangeP(xp, X, Y);
    end
    e1 = max(abs(yy - f(xx))); %вычисляем норму
    e2 = max(abs(yyt-f(xp)));
    E1 = [E1 e1]; %записываем ошибку в массив
    E2 = [E2 e2];
end

figure
semilogy(5:6:N,E1, "g.-")
title("Зависимость ошибки интерполяции полиномом Лагранжа 1 для р/м сетки")
xlabel("Степень полинома n")
ylabel("Ошибка интерполяции")
hold all
grid on 
semilogy(5:6:N,E2, "b.-")

%%иллюстрация работы интерполяции полинома Лагранжа, +поточечная 
clc, clear all, close all

%функция 1 с синусом
%равномерная сетка
x = -2:0.1:2;
XX = -2:0.1:2;
YY = [];
x0 = -1.5;
n = 41;
xp = Thicken(x, x0, n);

f = @(x) 2 .^ (sin(x));
X = linspace(-2,2,n); %количество узлов = 10
Y = f(X);
XP = Thicken(X, x0, n);
YT = f(XP);
for i = 1:n
    YY(i) = LagrangeP(XX(i), X, Y); %вычисляем полином
    YYT(i) = LagrangeP(xp(i), XP, YT);
end
YY;
YYT;
figure
plot(XX, YY, "b.-")
hold all
plot(xp, YYT, "g.-")
grid on