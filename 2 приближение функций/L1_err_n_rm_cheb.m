%зависимость ошибки интерполяции полиномом Лагранжа от степени полинома (колчество узлов)
clc, close all, clear all

%функция 1 с сигнумом
%равномерная сетка  
xx = -1:0.01:1;
YY = zeros(size(xx));
E = [];
N = 100; %колчество узлов
f = @(x) 3.*sign(x).*(x.^4)+4.*(x.^3)-12.*(x.^2)-5;

for n = 5:N
    X = linspace(-1,1,n);
    Y = f(X);
    for i = 1:length(xx)
        yy = LagrangeP(xx,X,Y);
    end
    e = max(abs(yy - f(xx))); %вычисляем норму
    E = [E e]; %записываем ошибку в массив
end

figure
semilogy(5:N,E, ".-")
title("Зависимость ошибки интерполяции полиномом Лагранжа 1 для р/м сетки")
xlabel("Степень полинома n")
ylabel("Ошибка интерполяции")
grid on 


clc, close all, clear all
%чебышевская сетка
xx = -1:0.01:1;
YY = zeros(size(xx));
E = [];
N = 100; %колчество узлов
f = @(x) 3.*sign(x).*(x.^4)+4.*(x.^3)-12.*(x.^2)-5;
a=-1;
b=1;
for n = 5:N
        XX=[];
        for k = 1:n
            i =(1./2).*(a+b) + (1./2).*(b-a).*cos( (2*k - 1 ) / (2*n) * pi);
            XX = [XX i];
        end
        Y = f(XX);
    for i = 1:length(xx)
        yy = LagrangeP(xx, XX, Y);
    end
    e = max(abs(yy - f(xx)));
    E =[E e];
end 

figure
semilogy(5:N, E , '.-')
title("Зависимость ошибки интерполяции полиномом Лагранжа 1 для сетки Чебышева")
xlabel("Степень полинома n")
ylabel("Ошибка интерполяции")
grid on 