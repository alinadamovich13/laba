clc, close all, clear all
%2 dots

x1 = 0.74;
f = @(x) 2 .^ (sin(x));
N = 250;
ee = [];
figure
for n = 5:N
    X = linspace(-2,2,n);
    Y = 2 .^(sin(X));
    TT = Ermit(X,Y);
    for j = 1 : length(X)-1
        if x1<=X(j+1) && x1>=X(j)
            E = TT(j,1).*(x1-X(j)).^3 + TT(j,2).*(x1-X(j)).^2 + TT(j,3).*(x1-X(j)) + TT(j,4);
        end
    end
   e = abs(E - f(x1));
   ee = [ee e];
end
semilogy(5:N,ee)
title("Зависимость ошибки в 1 точке функции 2 от количества узлов Эрмитов сплайн")
hold on
grid on

clc, close all, clear all
x2 = -0.15;
f = @(x) 2 .^ (sin(x));
N = 250;
ee = [];
figure
for n = 5:N
    X = linspace(-2,2,n);
    Y = 2 .^(sin(X));
    TT = Ermit(X,Y);
    for j = 1 : length(X)-1
        if x2<=X(j+1) && x2>=X(j)
            E = TT(j,1).*(x2-X(j)).^3 + TT(j,2).*(x2-X(j)).^2 + TT(j,3).*(x2-X(j)) + TT(j,4);
        end
    end
   e = abs(E - f(x2));
   ee = [ee e];
end
semilogy(5:N,ee)
title("Зависимость ошибки в 2 точке функции 2 от количества узлов Эрмитов сплайн")
hold on
grid on