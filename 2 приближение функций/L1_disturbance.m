clc, close all, clear all
%зависимость относительной погрешности от возмущений

xx =-2:0.02:2;
f = @(x) 2 .^ (sin(x));
yy = zeros(size(xx));
E = [];

for b = 1:5
X = linspace(-2, 2, 10);
Y = (f(X)).*(1+(1+rand*b/100));
    for i = 1:length(xx)
        yy(i) = LagrangeP(xx(i), X, Y);
    end 
e = max(abs((yy - f(xx))/abs(f(xx))));
E =[E e];
end

plot(1:5,E, "o-")
title("Зависимость относительной ошибки для интерполяции полиномом Лагранжа функции 1 от возмущения")
xlabel("Процент возмущения")
ylabel("Относительная ошибка")
grid on