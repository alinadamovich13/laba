clc, clear all, close all
format long
%x = linspace(-1, 3, 20);
%y = x .^ 3 - 4*x.^2 + 2*x +5;
%figure
%plot(x, y, 'o-')

%splinetool
%splinetool(x, y)
err = [];
for n = 5:250
    x = linspace(0, pi, n); %n - количество точек
    y = sin(x);
    % xx = linspace(x(1), x(end), 100);
    % yy = interp1(x, y, xx, 'spline')
    % hold all
    % plot(xx,yy)
    % grid on

    sp = csape(x, y, 'periodic'); %коэффициенты сплайна, 'clamped' | 'complete' | 'not-a-knot' | 'periodic' | 'second' | 'variational'
    %yZero = zeros(1,length(y))
    xx = linspace(x(1), x(end), n+10);
    yy = fnval(sp, xx); %значения сплайна функции csape
    err(n) = max(abs(yy - sin(xx)));
end

err

semilogy(1:250, err, ".-")
title("График зависимости ошибки для sin(x) в зависимости от шага р/м сетки")
xlabel("n - шаг равномерной сетки")
ylabel("модуль максимального уклонения")
grid on
