clc, close all, clear all
%зависимость относительной погрешности от возмущений

xx =-1:0.01:1;
f = @(x) 3.*sign(x).*(x.^4)+4.*(x.^3)-12.*(x.^2)-5;
yy = zeros(size(xx));
E = [];

for b = 1:5
X = linspace(-1, 1, 10);
Y = (f(X)).*(1+(1+rand*b/100));
    for i = 1:length(xx)
        yy(i) = LagrangeP(xx(i), X, Y);
    end 
e = max(abs((yy - f(xx))/abs(f(xx))));
E =[E e];
end

plot(1:5,E, "o-")
title("Зависимость относительной ошибки для интерполяции полиномом Лагранжа функции 2 от возмущения")
xlabel("Процент возмущения")
ylabel("Относительная ошибка")
grid on