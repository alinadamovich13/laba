y = 2 .^ (sin(x));
x = linspace(-5, 5);
figure
plot(x, y)
title("f1(x) = 2^(sin(x))")
grid on


x = linspace(-1, 1);
y = 3.*sign(x).*(x.^4)+4.*(x.^3)-12.*(x.^2)-5;
figure
plot(x, y)
title("f2(x) = 3.*sign(x).*(x.^4)+4.*(x.^3)-12.*(x.^2)-5")
grid on