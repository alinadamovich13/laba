clc, clear all, close all;
x = linspace(-5, 5);
f = x.^3 - 3*x.^2 - 3*x + 11;
plot(x, f);
grid minor
xlabel('x', 'Color','r')
ylabel('y', 'Color','b')
title('График функции 1')
legend('x^3 - 3*x^2 - 3*x + 11')