%код функции для бисекции, поиск корня уравнения f(x) = 0 с заданной
%точностью

function[arr, n, x] = methodBisection(f, a, b, eps)
    flag = true;
    x1 = a;
    x2 = b;
    n = 0;
    f1 = f(x1);
    f2 = f(x2);
    arr = zeros();

    if (f2*f1 > 0)
            disp(0); 
    end

    while(flag)
        c = (x1 + x2) / 2; %считаем середину отрезка
        arr(n+1) = c; %т.к. у нас n=0 изначально, то делаем n+1 чтобы записать в 1 элемент массива
        f3 = f(c);

        if ((f1*f3) < 0)
            x2 = c;
            f2 = f3;
        else
            x1 = c;
            f1 = f3;
        end
        n = n + 1; %увеличиваем n
        flag = abs(x1-x2) > eps; %условие выхода из цикла
        
    end
    x = c(end)
 

end
